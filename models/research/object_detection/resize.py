import cv2

img=cv2.imread('test1.jpg')
scale_percent=3.50
width=250
height=250
dimension=(width,height)

resized=cv2.resize(img,dimension,interpolation=cv2.INTER_AREA)
print(resized.shape)
cv2.imshow('ouput',resized)
cv2.imwrite('resized_test1.jpg',resized)

cv2.waitKey(0)
cv2.destoryAllWindows()
